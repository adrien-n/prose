Introduction
============

This is the contribution guide to Prose, a collaborative text editor.

There is little process involved and contributions are welcome on all topics (UI, SQL, Continuous Integration, all features, ...).

Communication channels
-------

Being new, dedicated communication channels have not been setup yet. Currently, the most appropriate are:
* #ocaml and #ocaml-fr on irc.freenode.net
* some channels on geeknode (if you know geeknode, you know which channels)
* the Prose bugtracker

Supported platforms
-------

Prose is new and thus targets new browsers or at least new enough. This means that whatever is available in 2017 should be supported.

Mobile devices are also a support target but are secondary ones. This is in acknowledgement to the difficulty in fitting the UI on a phone screen.

The editing library: QuillJS
-------

Prose uses [QuillJS](https://quilljs.com) for the text-editing component. It therefore takes advantages of its features but also inherits its bugs.

Every issue that is centered around single-user editing should be tried against the demos on the [QuillJS website](https://quilljs.com) before being filled on the Prose bugtracker.

Support
========

If you need support with installing or using Prose, you should create a bug report on gitlab. The project is still new and hasn't seen the need for a dedicated communication channel yet.

Alternatively, if you use IRC and speak French, you can hop on #ocaml-fr on Freenode. This is not the channel's primary topic but should be fine for low volumes.

Bug reports
-------

Bug reports should be done on the gitlab bugtracker for Prose. They need to include the setup used (typically, OS and browser used) and specify steps to reproduce the issue.

If connectivity was unreliable when the bug occurred, this *must* be mentioned.

Code contributions
=========

Style
-------

Changes should be similar to the rest of the file.

If the rest of the file has a weird style, it can be OK to change it first in a separate commit. For such situations, it is better to ask for another point-of-view first.

Workflow
------

Changes should be done as Merge Requests on Gitlab.

The code is heavily commented and should be kept that way. It can be OK to delay documentation updates if a change is important but documentation be must updated soon enough afterwards.

Commit message need to follow the typical Git commit guidelines: one short description on the first line and a full explanation in the body below (i.e. what was broken, what was the need, what has been changed, plus « This fixes #42. » or similar).

Licensing
---------

Prose is licensed under the AGPLv3. As such your contributions need to be under a compatible license (i.e. BSD-like is fine).

This will not change. Nor will contributor agreements be required.
