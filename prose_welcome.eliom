[%%client
open Prose_types

let t ?a text =
  let b = Buffer.create (2 * String.length text) in
  Prose_formats.recode ~encoding:`UTF_8 `UTF_16LE (`String text) (`Buffer b);
  let bs = Buffer.contents b in
  let text = Array.init (Buffer.length b / 2) (fun i -> Char.code bs.[2*i] + 256 * Char.code bs.[2*i + 1]) in
  Insert (Insert.(Text { insert = text; attributes = a }))

open Attributes

let title = { default with header = Some 1; align = Some Align.Center }
let section = { default with header = Some 2 }
let bullet = { default with list = Some Lst.Bullet }
let color s = Some (Color.import s)

let message = [
  t ~a:title "Welcome to Prose!\n";

  t ~a:section "Collaborative Editing\n";
    t "This is a collaborative editing system based on Quill for the editing widget and Ocsigen for everything else.\n";
    t "Every change is propagated to every one else without latency and this makes it possible to write a document with many hands at the same time.\n";
    t "Jot notes down during a meeting, improve grammar and wording iteratively, add comments or extend sections. All of this can be done by different participants without blocking each other.\n";
    t "\n";

  t ~a:section "Rich-text Support\n";
    t "Rich-text means you are not limited to raw text. You can:\n";
      t ~a:bullet "Add bullet points,\n";
      t ~a:{ bullet with bold = Some true } "Write in bold,\n";
      t ~a:{ bullet with italic = Some true } "or in italics,\n";
      t ~a:{ bullet with size = Some Size.Large } "in various sizes,\n";
      t ~a:{ bullet with color = color "#4F8F4F" } "and colors,\n";
      t ~a:{ bullet with color = color "#2F6F2F"; background = color "#CFAFCF" } "including background ones,\n";
      t ~a:{ bullet with script = Some Script.Sub } "and";
      t ~a:{ bullet with script = None } " ";
      t ~a:{ bullet with script = Some Script.Super } "more";
      t ~a:{ bullet with script = None } ".\n";
    t "\n";

  t ~a:section "Unicode and emojis\n";
    t "You can input anything you want, including emojis and combination of emojis.\n";
    t "Obviously, this includes the Unicode Snowman: ";
    t ~a:{ default with size = Some Size.Large } "⛄.\n";
    t "It also includes composed emojis: ";
    t ~a:{ default with size = Some Size.Large } "🏳️ + 🌈 = 🏳️‍🌈.\n";
    t "\n";

  t ~a:section "Free Software\n";
    t "Prose is free software: you can redistribute it and/or modify it under ";
    t "the terms of the GNU Affero General Public License as published by the ";
    t "Free Software Foundation, either version 3 of the License, or (at your ";
    t " option) any later version.\n";
    t "The project is developed on ";
    t ~a:{ default with link = Some "https://gitlab.com/adrien-n/prose" } "GitLab";
    t " and its license can be read ";
    t ~a:{ default with link = Some "https://gitlab.com/adrien-n/prose/blob/master/LICENSE" } "online";
    t ".\n";
]

]

[%%server

let about =
  let open Eliom_content.Html.F in
  div ~a:[ a_class [ "about" ] ] [
    h1 ~a:[ a_style "text-align: center;" ] [ pcdata "Welcome to Prose!" ];
    h2 [ pcdata "Collaborative Editing" ];

    p [ pcdata "This is a collaborative editing system based on Quill for the editing widget and Ocsigen for everything else." ];
    p [ pcdata "Every change is propagated to every one else without latency and this makes it possible to write a document with many hands at the same time." ];
    p [ pcdata "Jot notes down during a meeting, improve grammar and wording iteratively, add comments or extend sections. All of this can be done by different participants without blocking each other." ];

    h2 [ pcdata "Rich-text Support\n" ];
    p [ pcdata "Rich-text means you are not limited to raw text. You can:" ];
    ul [
      li [ pcdata "Add bullet points," ];
      li ~a:[ a_style "font-weight: bold;" ] [ pcdata "Write in bold," ];
      li ~a:[ a_style "font-style: italic;" ] [ pcdata "or in italics," ];
      li ~a:[ a_style "font-size: 120%;" ] [ pcdata "in various sizes," ];
      li ~a:[ a_style "color: #4F8F4F;" ] [ pcdata "and colors,\n" ];
      li ~a:[ a_style "color: #2F6F2F; background-color: #CFAFCF;" ] [ pcdata "including background ones," ];
      li [
        span ~a:[ a_style "vertical-align: sub;"] [ pcdata "and" ];
        span ~a:[ a_style "" ] [ pcdata " " ];
        span ~a:[ a_style "vertical-align: super;"] [ pcdata "more" ];
        span ~a:[ a_style "" ] [ pcdata "." ];
      ]
    ];

    h2 [ pcdata "Unicode and emojis\n" ];
    p [ pcdata "You can input anything you want, including emojis and combination of emojis.\n" ];
    p [
      pcdata "Obviously, this includes the Unicode Snowman: ";
      span ~a:[ a_style "font-size: 120%;" ] [ pcdata "⛄." ];
      br ();
      span [ pcdata "It also includes composed emojis: " ];
      span ~a:[ a_style "font-size: 120%;" ] [ pcdata "🏳️ + 🌈 = 🏳️‍🌈." ];
    ];

    h2 [ pcdata "Free Software\n" ];
    p [
      span [
        pcdata "Prose is free software: you can redistribute it and/or modify it under ";
        pcdata "the terms of the GNU Affero General Public License as published by the ";
        pcdata "Free Software Foundation, either version 3 of the License, or (at your ";
        pcdata " option) any later version."
      ]
    ];
    p [
     span [
       pcdata "The project is developed on ";
       Raw.(a ~a:[ a_href (uri_of_string "https://gitlab.com/adrien-n/prose") ] [ pcdata "GitLab" ]);
       pcdata " and its license can be read ";
       Raw.(a ~a:[ a_href (uri_of_string "https://gitlab.com/adrien-n/prose/blob/master/LICENSE") ] [ pcdata "online" ]);
       pcdata ".";
     ]
    ]
  ]
]
