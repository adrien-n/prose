[%%client
  module Attributes = struct
    module Color = struct
      type rgb = { red : int; green : int; blue : int } [@@deriving json, yojson]
      type hsl = { hue : int; saturation : int; lightness : int } [@@deriving json, yojson]
      type hsv = { hue : int; saturation : int; value : int } [@@deriving json, yojson]
      type t =
        | RGB of rgb
        | HSL of hsl
        | HSV of hsv
        | None
      [@@deriving json, yojson]
      let export = function
        | None -> ""
        | RGB rgb ->
            Printf.sprintf "#%06x" (rgb.red * (256 * 256) + rgb.green * 256 + rgb.blue)
        | _ -> assert false
      let import = function
        | "none" -> None
        | o ->
            let i = Scanf.sscanf o "#%x" (fun i -> i) in
            RGB { red = i / (256 * 256); green = (i / 256) mod 256; blue = i mod 256 }
    end

    module Align = struct
      type t =
        | Center
        | Justify
        | Right
        | Left (* NB: I think this one doesn't actually exist: null is used instead *)
        | None
      [@@deriving json, yojson]
      let export = function
        | Center -> "center"
        | Justify -> "justify"
        | Right -> "right"
        | Left -> "left"
        | None -> "none"
      let import = function
        | "center" -> Center
        | "justify" -> Justify
        | "right" -> Right
        | "left" -> Left
        | "none" -> None
        | _ -> assert false
    end

    module Direction = struct
      type t =
        | RTL
        | LTR
        | None
      [@@deriving json, yojson]
      let export = function
        | RTL -> "rtl"
        | LTR -> "ltr"
        | None -> "none"
      let import = function
        | "rtl" -> RTL
        | "ltr" -> LTR
        | "none" -> None
        | _ -> assert false
    end

    module Lst = struct
      type t =
        | Ordered
        | Bullet
        | None
      [@@deriving json, yojson]
      let export = function
        | Ordered -> "ordered"
        | Bullet -> "bullet"
        | None -> "none"
      let import = function
        | "ordered" -> Ordered
        | "bullet" -> Bullet
        | "none" -> None
        | _ -> assert false
    end

    module Script = struct
      type t =
        | Sub
        | Super
        | None
      [@@deriving json, yojson]
      let export = function
        | Sub -> "sub"
        | Super -> "super"
        | None -> "none"
      let import = function
        | "sub" -> Sub
        | "super" -> Super
        | "none" -> None
        | _ -> assert false
    end

    module Size = struct
      type t =
        | Small
        | Normal
        | Large
        | Huge
      [@@deriving json, yojson]
      let export = function
        | Small -> "small"
        | Normal -> "normal"
        | Large -> "large"
        | Huge -> "huge"
      let import = function
        | "small" -> Small
        | "normal" -> Normal
        | "large" -> Large
        | "huge" -> Huge
        | _ -> assert false
    end

    type url = string [@@deriving json, yojson]

    type t = {
      align : Align.t option;
      background : Color.t option;
      blockquote : bool option;
      bold : bool option;
      code : bool option;
      code_block : bool option;
      color : Color.t option;
      (* direction : Direction.t option; *)
      font : string option;
      header : int option;
      image : string option;
      indent : int option;
      italic : bool option;
      link : string option;
      list : Lst.t option;
      (* list/item *)
      script : Script.t option;
      size : Size.t option;
      strike : bool option;
      underline : bool option;
      video : string option;
    } [@@deriving json, yojson]

    let default = {
      align = None;
      background = None;
      blockquote = None;
      bold = None;
      code = None;
      code_block = None;
      color = None;
      (* direction = None; *)
      font = None;
      header = None;
      image = None;
      indent = None;
      italic = None;
      link = None;
      list = None;
      (* list/item *)
      script = None;
      size = None;
      strike = None;
      underline = None;
      video = None;
    } [@@deriving json, yojson]
  end

  module Insert = struct
    type text = {
      insert : int array;
      attributes : Attributes.t option;
    } [@@deriving json, yojson]

    (* type embed = {
      url : Attributes.url;
      link : Attributes.url option;
    } [@@deriving json, yojson] *)

    type t =
      | Text of text
      (* | Image of embed
      | Video of embed *)
    [@@deriving json, yojson]
  end

  module Delete = struct
    type t = {
      delete : int;
    } [@@deriving json, yojson]
  end

  module Retain = struct
    type t = {
      retain : int;
      attributes : Attributes.t option;
    } [@@deriving json, yojson]
  end

  type op =
    | Insert of Insert.t
    | Delete of Delete.t
    | Retain of Retain.t
  [@@deriving json, yojson]

  let may f o =
    match o with
    | Some o -> f o
    | None -> ()

  let may_map f o =
    match o with
    | Some o -> Some (f o)
    | None as o -> o

  module Hierarchical = struct
    module A = Attributes

    type phrasing_attrs = {
      background : A.Color.t option;
      bold : bool option;
      color : A.Color.t option;
      italic : bool option;
      script : A.Script.t option;
      size : A.Size.t option;
      strike : bool option;
      underline : bool option;
    }

    type phrasing = string * phrasing_attrs

    type block_attrs = {
      align : A.Align.t option;
    }

    type block =
      | Blockquote of phrasing list * block_attrs
      | Empty
      | Header of int * phrasing list * block_attrs
      | List of A.Lst.t * phrasing list list * block_attrs
      | P of phrasing list * block_attrs
  end
]

[%%shared
  module OpaqueBase = struct
    type storage = string [@@deriving json, yojson]
  end
]

[%%server
  module Opaque : sig
    type t = OpaqueBase.storage [@@deriving json, yojson]
  end = struct
    type t = OpaqueBase.storage [@@deriving json, yojson]
  end
]

[%%client
  module Opaque : sig
    type t = OpaqueBase.storage [@@deriving json, yojson]
    val from_t : op -> t
    val to_t : t -> op
  end = struct
    type t = OpaqueBase.storage [@@deriving json, yojson]
    external from_t : op -> t = "%identity"
    external to_t : t -> op = "%identity"
  end
]

[%%shared
  type edit_id = int64 [@@deriving json, yojson]

  type base_id = int64 [@@deriving json, yojson]

  type author = int [@@deriving json, yojson]

  type up_edit = base_id * Opaque.t [@@deriving json, yojson]

  type up = [
    | `Edit of up_edit
    | `GetBacklog
  ] [@@deriving json, yojson]

  type condensate = edit_id * Opaque.t [@@deriving json, yojson]

  type 'a down_generic = {
    base_id : base_id;
    author : author;
    edit : 'a;
  } [@@deriving json, yojson]

  type down = [
    | `Edit of Opaque.t down_generic
    | `Backlog of (int64 * Opaque.t option * (base_id * author * string) list)
  ] [@@deriving json, yojson]

  type client = {
    up : up_edit React.event;
    down : unit React.event;
    condensate_up : condensate Eliom_react.Up.t;
    edit_event : Opaque.t down_generic React.event;
  }
]

[%%client
  type ('a, 'b) state_generic = {
    epoch : edit_id;
    consensus : 'b;
    history : 'a down_generic CCFQueue.t;
    local_deltas : (float * 'a down_generic) CCFQueue.t;
  }

  let id_of_state { epoch; history } =
    Int64.(add epoch (of_int @@ CCFQueue.size history))

  let history_since state ~since =
    let consensus_id = id_of_state state in
    let distance = max 0 Int64.(to_int (sub consensus_id since)) in
    snd (CCFQueue.take_back_l distance state.history)
]
