[%%server
module Quill = struct
  module Delta = struct
    type document
  end
  module Quill = struct
    type t
  end
end
]

[%%client
  open Prose_types

  let recode ?nln ?encoding out_encoding
  (* copied from Uutf's doc *)
      (src : [`Channel of in_channel | `String of string])
      (dst : [`Channel of out_channel | `Buffer of Buffer.t])
    =
    let rec loop d e = match Uutf.decode d with
    | `Uchar _ as u -> ignore (Uutf.encode e u); loop d e
    | `End -> ignore (Uutf.encode e `End)
    | `Malformed _ -> ignore (Uutf.encode e (`Uchar Uutf.u_rep)); loop d e
    | `Await -> assert false
    in
    let d = Uutf.decoder ?nln ?encoding src in
    let e = Uutf.encoder out_encoding dst in
    loop d e

  (* Conversion functions are defined on the client side only. It is not
   * possible to use the Js module server-side so we have to do everything on
   * the client. *)

  (* Javascript uses UTF-16 for its strings. We need to do some string
   * processing on the server too and therefore we can't handle strings as
   * opaque. We don't need to do much but we need to be able to get substrings
   * and concatenate strings. Fortunately, Quill and JS use indexes in the
   * UTF-16 stream without concerning themselves with visible glyphs or
   * anything: they just use it as a dumb array. We can therefore use an array
   * of 16 bits integers on the OCaml side and convert from and to JS very
   * easily.
   *
   * NOTE: I think this assumes UTF-16 little-endian. *)
  module StringAsArray = struct
    type t = int array (* each element can be 16 bits *)

    (* Convert a js_string into t. This creates an OCaml int array of the same
     * length as the JS string and each item is the charCode at the
     * corresponding index in the JS string. *)
    let of_js_string (s : Js.js_string Js.t) : t =
      (* The ##. operator below lets us access the length property of the JS
       * object. *)
      Array.init s##.length (fun i ->
        (* The ## operator below lets us call the charCodeAt method of the JS
         * object. It returns a float because out-of-bound accesses return NaN!
         * We cannot access the string outside of its bounds so we directly
         * convert the result to an int. *)
        truncate (s##charCodeAt i)
      )

    (* Convert a 16 bits integer to a JS string. Actually we want a "character"
     * but these don't exist in JS and strings of length 1 are used instead. *)
    let int16_to_js_string i =
      (* [Js.Unsafe.global##._String] returns the constructor from the String
       * object and we then use the [fromCharCode] static method to create a new
       * object populated with the char code [i]. *)
      Js.Unsafe.global##._String##fromCharCode i

    (* Convert a t to a js_string.
     * This creates an empty js_string and iterates of the OCaml array to
     * concatenate each char code to it. *)
    let to_js_string (t : t) =
      let s = ref (Js.string "") in
      for i = 0 to Array.length t - 1 do
        s := (!s)##concat (int16_to_js_string t.(i))
      done;
      !s
  end

  module Quill = struct

    module Delta : sig
      class type op = object
        method insert : string Js.t Js.opt Js.readonly_prop
        method retain : int Js.opt Js.readonly_prop
        method delete : int Js.opt Js.readonly_prop
      end
      class type o = object('self)
        method ops : op Js.t Js.js_array Js.t Js.readonly_prop
        method compose : 'self Js.t -> 'self Js.t Js.meth
        method transform : 'self Js.t -> 'self Js.t Js.meth
        method transformPosition : int -> int Js.meth
        method diff : 'self Js.t -> 'self Js.t Js.meth
      end
      type 'a t = o Js.t
      type edit = [ `Edit ] t
      type document = [ `Document ] t
      val delta_js : Js.Unsafe.any
      val empty_edit : edit
      val empty_state : (edit, document) state_generic
      val of_string : string -> 'a t
      val to_string : 'a t -> string
      val rebase : edit down_generic -> (edit, document) state_generic -> edit down_generic
      val compose_edits_on_document : init:document -> q:edit down_generic CCFQueue.t -> document
      val compose_edits : edit down_generic CCFQueue.t -> edit option
    end = struct
      class type op = object
        method insert : string Js.t Js.opt Js.readonly_prop
        method retain : int Js.opt Js.readonly_prop
        method delete : int Js.opt Js.readonly_prop
      end
      class type o = object('self)
        method ops : op Js.t Js.js_array Js.t Js.readonly_prop
        method compose : 'self Js.t -> 'self Js.t Js.meth
        method transform : 'self Js.t -> 'self Js.t Js.meth
        method transformPosition : int -> int Js.meth
        method diff : 'self Js.t -> 'self Js.t Js.meth
      end
      type 'a t = o Js.t
      type edit = [ `Edit ] t
      type document = [ `Document ] t

      (* We need to create Delta objects but the "module" is not directly visible
       * because of webpack. Fortunately, quill offers an API to access its
       * dependencies and we can "import" the corresponding module to be able to
       * call "new Delta" later on. *)
      let delta_js = Js.Unsafe.(inject @@ eval_string "Quill.import('delta')")
      let empty_edit = Js.Unsafe.new_obj delta_js [||]
      let of_string s : edit = empty_edit##compose Js.(_JSON##parse (string s))
      let empty_document = of_string "{ \"ops\": [ { \"insert\": \"\\n\" } ] }"
      let to_string (d : edit) = Js.(to_string (_JSON##stringify d))

      let empty_state = {
        epoch = 0L;
        consensus = empty_document;
        history = CCFQueue.empty;
        local_deltas = CCFQueue.empty;
      }

      let rebase down state =
        let l = history_since state ~since:down.base_id in
        let consensus_id = id_of_state state in
        let delta = ListLabels.fold_left l ~init:down.edit ~f:(fun delta consensus_edit ->
          if down.author <> consensus_edit.author then
            consensus_edit.edit##transform delta
          else
            delta
        )
        in
        { down with edit = delta; base_id = consensus_id }

      let compose_edits_on_document ~init ~q =
        CCFQueue.fold (fun c d -> c##compose d.edit) init q

      let compose_edits q =
        match CCFQueue.take_front q with
        | Some ((c0 : edit down_generic), q) ->
            Some (CCFQueue.fold (fun c d -> c##compose d.edit) c0.edit q)
        | None ->
            None
    end

    type state = (Delta.edit, Delta.document) state_generic

    module Selection = struct
      class type o = object
        method index : int Js.readonly_prop
        method length : int Js.readonly_prop
      end
      type t = o Js.t

      let make ~index ~length =
        object%js
					val index = index
					val length = length
        end

      let compose (t : t) (delta : Delta.edit) =
        let djs = Js.Unsafe.new_obj Delta.delta_js [| Js.Unsafe.inject delta |] in
        let index = djs##transformPosition t##.index in
        let length = djs##transformPosition (t##.index + t##.length) - index in
        object%js
          val index = index
          val length = length
        end

      let rebase (t : t) ~state ~ourselves ~old_base =
        let l = history_since state ~since:old_base in
        let i, j = ListLabels.fold_left
          l
          ~init:(t##.index, t##.index + t##.length)
          ~f:(fun (i, j) { author; edit } ->
            if ourselves <> author then
              edit##transformPosition i
              , edit##transformPosition j
            else
              i, j
          )
        in
        make ~index:i ~length:(j - i)
    end

    module Options = struct
      class type modules = object
        method toolbar : Js.js_string Js.t Js.opt Js.readonly_prop
        method syntax : bool Js.t Js.readonly_prop
      end
      class type o = object
        method modules : modules Js.t Js.readonly_prop
        method placeholder : Js.js_string Js.t Js.readonly_prop
        method readOnly : bool Js.readonly_prop
        method theme : Js.js_string Js.t Js.readonly_prop
      end
      type t = o Js.t
    end

    module Quill = struct
      class type o = object
        method getSelection : unit -> Selection.t Js.opt Js.meth
        method setSelection : Selection.t -> unit Js.meth
        method setContents : Delta.document -> unit Js.meth
        method getContents : unit -> Delta.document Js.meth
        method updateContents : Delta.document -> Delta.edit Js.meth
        method on : Js.js_string Js.t -> (Delta.edit -> Delta.edit -> Js.js_string Js.t -> unit Lwt.t) Js.callback -> unit Js.meth
      end
      type t = o Js.t
      let t (container : [> Html_types.div ] Eliom_content.Html.D.elt) (options : Options.t) : t =
        let container2 = Eliom_content.Html.To_dom.of_element container in
        (* Return the constructor for Quill objects, this will make it possible to
         * do "new Quill" from OCaml. Note the leading underscore. *)
        let constructor = Js.Unsafe.global##._Quill in
        new%js constructor container2 options

      let to_attributes a =
        let open Js in
        let f_int o =
          Js.Optdef.to_option o
          |> may_map (fun x -> Js.Opt.get x (fun () -> 0))
        in
        let f_string o =
          Js.Optdef.to_option o
          |> may_map (fun x -> Js.Opt.get x (fun () -> string "none"))
          |> may_map Js.to_string
        in
        let f_translate g o =
          f_string o
          |> may_map g
        in
        let f_bool o =
          Js.Optdef.to_option o
          |> may_map Js.to_bool
        in
        Attributes.{
          align = f_translate Align.import a##.align;
          background = f_translate Color.import a##.background;
          blockquote = f_bool a##.blockquote;
          bold = f_bool a##.bold;
          code = f_bool a##.code;
          code_block = f_bool a##.code_block;
          color = f_translate Color.import a##.color;
          (* direction = f_translate Direction.import a##.direction; *)
          font = f_string a##.font;
          header = f_int a##.header;
          image = f_string a##.image;
          indent = f_int a##.indent;
          italic = f_bool a##.italic;
          link = f_string a##.link;
          list = f_translate Lst.import a##.list;
          script = f_translate Script.import a##.script;
          size = f_translate Size.import a##.size;
          strike = f_bool a##.strike;
          underline = f_bool a##.underline;
          video = f_string a##.video;
        }

      let to_delta delta =
        let to_option = Js.Optdef.to_option in
        let js_bind f o =
          match to_option o with
          | Some o -> Some (f o)
          | None -> None
        in
        Js.to_array (Js.Unsafe.coerce delta)##.ops
        |> ArrayLabels.fold_left
          ~init:[]
          ~f:(fun acc op ->
            let op = Js.Unsafe.coerce op in
            let insert = to_option op##.insert in
            let retain = to_option op##.retain in
            let delete = to_option op##.delete in
            match insert, retain, delete with
            | Some insert, None, None ->
                let caml_op = Insert Insert.(Text {
                  insert = StringAsArray.of_js_string insert;
                  attributes = js_bind to_attributes op##.attributes;
                })
                in
                caml_op :: acc
            | None, Some retain, None ->
                let caml_op = Retain Retain.{
                  retain = retain;
                  attributes = js_bind to_attributes op##.attributes;
                }
                in
                caml_op :: acc
            | None, None, Some delete ->
                let caml_op = Delete Delete.{
                  delete = delete;
                }
                in
                caml_op :: acc
            | _ ->
                assert false (* XXX *)
        )
        |> List.rev
    end

  end

  (* HTML export module *)
  module HTML = struct
    open Eliom_content.Html.D

    module Style = struct

      (* For a list of (string * bool) list, return a style attribute made of
       * the strings for which the accompanying bool was true. *)
      let a_style l =
        (* Return the list of 'a for which the accompanying boolean is true. *)
        let l =
          ListLabels.fold_left l ~init:[] ~f:(fun acc (style, b) ->
            if b then
              style :: acc
            else
              acc
          )
        in
        match l with
        | [] -> []
        | l -> [ a_style (String.concat "; " l) ]

      (* List of style attributes and accompanying criteria for an inline element. *)
      let phrasing a = Hierarchical.[
        "font-size: small",               a.size = Some Attributes.Size.Small;
        "font-size: normal",              a.size = Some Attributes.Size.Normal;
        "font-size: large",               a.size = Some Attributes.Size.Large;
        "font-size: x-large",             a.size = Some Attributes.Size.Huge;
        "font-weight: bold",              a.bold = Some true;
        "font-style: italic",             a.italic = Some true;
        "text-decoration: underline",     a.underline = Some true && a.strike <> Some true;
        "text-decoration: line-through",  a.underline <> Some true && a.strike = Some true;
        "text-decoration: underline line-through", a.underline = Some true && a.strike = Some true;
        "vertical-align: sub",            a.script = Some Attributes.Script.Sub;
        "vertical-align: sup",            a.script = Some Attributes.Script.Super;
      ]

      (* List of style attributes and accompanying criteria for a block element. *)
      let block a = Hierarchical.[
        "text-align: left",     a.align = Some Attributes.Align.Left;
        "text-align: right",    a.align = Some Attributes.Align.Right;
        "text-align: center",   a.align = Some Attributes.Align.Center;
        "text-align: justify",  a.align = Some Attributes.Align.Justify;
      ]
    end

    (* Create a <span> with the given attributes and holding the given text. *)
    let phrasing_to_html (text, attrs) =
      span ~a:Style.(a_style (phrasing attrs)) [ pcdata text ]

    (* Create a block using constructor with the attributes provided in
     * block_attrs and holding the inline elements from phrasings. *)
    let make_block ~constructor ~phrasings ~block_attrs  =
      ListLabels.rev_map phrasings ~f:phrasing_to_html
      |> List.rev
      |> constructor ?a:(Some Style.(a_style (block block_attrs)))

    (* Convert a single block in hierarchical form to an HTML element if there
     * should be one. *)
    let block_to_html block =
      let open Hierarchical in
      match block with
      | Empty ->
          None
      | Blockquote (phrasings, block_attrs) ->
          Some (make_block ~constructor:blockquote ~phrasings ~block_attrs)
      | Header (n, phrasings, block_attrs) ->
          let constructor = match n with
          | 1 -> h1
          | 2 -> h2
          | 3 -> h3
          | 4 -> h4
          | 5 -> h5
          | 6 -> h6
          | _ -> h6
          in
          Some (make_block ~constructor ~phrasings ~block_attrs)
      | List (k, phrasings_l, block_attrs) ->
          let lis = List.map (fun phrasings -> make_block ~constructor:li ~phrasings ~block_attrs) phrasings_l in
          Some Attributes.Lst.(match k with
          | Ordered -> ol lis
          | Bullet -> ul lis
          | None -> Printf.eprintf "warning: no list kind for list item.\n%!"; ul lis)
      | P (phrasings, block_attrs) ->
          Some (make_block ~constructor:p ~phrasings ~block_attrs)

    (* Output the whole HTML representation based on the hierarchical
     * representation h provided. *)
    let to_html h =
      (* Build a list of all toplevel block elements. *)
      ListLabels.fold_left h ~init:[] ~f:(fun acc block ->
        match block_to_html block with
        | Some e -> e :: acc
        | None -> acc
      )
      |> List.rev
  end

  (* Convert Quill's document representation to a hierarchical one that can be
   * used more easily.
   * This makes a number of assumptions and guesses. *)
  module ToHierarchical = struct
    (* Convert text encoded in UTF-16 and stored in an array to UTF-8. *)
    let text a =
      (* Each array cell holds a codepoint, i.e. we use 16 bits of the array
       * integers. *)
      let module B = Buffer in
      let b16 = B.create (2 * Array.length a) in
      for i = 0 to Array.length a - 1 do
        B.add_char b16 (Char.chr (a.(i) mod 256));
        B.add_char b16 (Char.chr (a.(i) / 256))
      done;
      let b8 = B.create (Array.length a) in
      recode ~encoding:`UTF_16LE `UTF_8 (`String (B.contents b16)) (`Buffer b8);
      B.contents b8

    (* Split a text insert on newlines because in some situations because there
     * are newlines in the middle of inserts and also because we need to spot
     * newlines easily.
     * Quill puts block attributes on "\n" characters but it's also possible to
     * have a single "insert" element with several blocks. For instance, an
     * insert of two lines with the second one being centered needs to be split
     * into two blocks so that the align attribute on the second newline does
     * not get used for the first one. *)
    let split_text ~acc { Insert.insert; attributes } =
      (* Convert the text to an UTF-8 string. *)
      let insert = text insert in
      let attributes = match attributes with Some a -> a | _ -> Attributes.default in
      (* The function below accumulates the lines and uses an integer to keep
       * track of its current position in the string. *)
      let rec aux ~acc ~i =
        (* If the position is at the end of the text or further than that,
         * return directly. *)
        if i >= String.length insert then
          acc
        else
          (* Look for the next '\n' character. If none is found, we'll go to
           * the end of the string while noting that there was no newline. *)
          let j, newline = try String.index_from insert i '\n', true with
            Not_found -> String.length insert, false
          in
          (* Extract the text leading up to right before the position found
           * above. *)
          let insert = String.sub insert i (j - i) in
          (* Empty strings are interpreted as purely new lines (that's the only
           * way to get an empty string here) and we need to know when such
           * situations arise. *)
          let insert = if insert = "" then "\n" else insert in
          (* Copy the inline attributes of this insert into each line. *)
          let acc = (insert, attributes) :: acc in
          (* If a newline was found and the current split isn't only a newline,
           * insert one. *)
          let acc = if newline && insert <> "\n" then ("\n", attributes) :: acc else acc in
          (* Finally, do the recursive call with the new attribute and the new
           * position in the input string. *)
          aux ~acc ~i:(j + 1)
      in
      aux ~acc ~i:0

    let linearize t =
      (* First split the text according to split_text, i.e. split text inserts
       * that have newlines in them in order to never have several lines in the
       * same insert.
       * The list is reversed after the fold_left and this property is used
       * right after. *)
      ListLabels.fold_left t
        ~init:[]
        ~f:(fun acc e ->
          (* Call should on each text insert.
           * There should be no Delete or Retain operation in the input
           * document because we work on the consensus document and not on the
           * edits. *)
          match e with
          | Insert Insert.Text t -> split_text ~acc t
          | (Delete _ | Retain _)  -> assert false
        )
      (* Fold over the document, starting from its end and copy the block
       * attributes to all the insertions that are on the same line.
       * Remember that block attributes are stored as attributes of the
       * trailing "\n" of each block so each time we find a "\n" character, we
       * copy its block attributes (i.e. list and alignment) to every
       * subsequent insert until we find another newline, at which point we
       * switch to using this newline's block attributes.
       * The accumulator holds the list and the align attributes (both are
       * options) and the result so far. *)
      |> ListLabels.fold_left
          ~init:((None, None), [])
          ~f:(fun ((inlist, align), l) ((text, attributes) as e) ->
            let module A = Attributes in
            (* If the current text is "\n", use its list and align attributes
             * for subsequent elements and push it to the result set, otherwise
             * push the text but with the attributes of the last linefeed that
             * has been seen. *)
            if text = "\n" then
              (attributes.A.list, attributes.A.align)
              , e :: l
            else
              (inlist, align)
              , (text, { attributes with A.list = inlist; align = align }) :: l
          )
      |> snd

    (* Helper function to create phrasing elements from Hierarchical based on
     * the given text and attributes. *)
    let phrasing (text, a) =
      if text = "\n" then
        []
      else
        [
          text, Hierarchical.{
            background = a.A.background;
            bold = a.A.bold;
            color = a.A.color;
            italic = a.A.italic;
            script = a.A.script;
            size = a.A.size;
            strike = a .A.strike;
            underline = a.A.underline;
          }
        ]

    let to_hierarchical (acc1, acc2) ((text, attributes) as e) =
      let ba = Hierarchical.{
        align = attributes.A.align;
      }
      in

      let open Attributes in
      let open Hierarchical in
      match attributes, acc1 with

      (* Blockquote *)
      (* Continuation of the current blockquote *)
      | { blockquote = Some true }, Blockquote (phrasings, ba1)
      when ba1 = ba ->
          Blockquote ((phrasing e @ phrasings), ba1), acc2
      (* A new blockquote. *)
      | { blockquote = Some true }, _ ->
          Blockquote (phrasing e, ba), acc1 :: acc2

      (* Header *)
      (* Continuation of the current header *)
      | { header = Some n }, Header (n1, phrasings, ba1)
      when n1 = n && ba1 = ba ->
          (* Concatenate all consecutive inline elements together. The end
           * delimiter is newline. *)
          if text <> "\n" then
            Header (n, phrasing e @ phrasings, ba), acc2
          else
            Empty, acc1 :: acc2
      (* A new header. *)
      | { header = Some n }, _ ->
          Header (n, phrasing e, ba), acc1 :: acc2

      (* List *)
      (* Continuation of the current list *)
      | { list = Some (Lst.Bullet as k1) }, List (Lst.Bullet as k2, phrasings, ba1)
      | { list = Some (Lst.Ordered as k1) }, List (Lst.Ordered as k2, phrasings, ba1)
      when k1 = k2 && ba1 = ba ->
          let l = match text, phrasings with
          (* Current text is a newline and there were already list items: begin
            * a new list item. *)
          | "\n", _ :: _ -> List (k1, [] :: phrasings, ba1)
          (* Regular text and there is at least one list item already created:
            * push the text to that list item. *)
          | _, hd :: tl -> List (k1, (phrasing e @ hd) :: tl, ba1)
          (* Regular text and there is no list item currently existing: create
           * a new list item with the current text. *)
          | _, _ -> List (k1, [ phrasing e ], ba1)
          in
          l, acc2
      (* A new list. *)
      | { list = Some (Lst.Bullet as k1) }, _
      | { list = Some (Lst.Ordered as k1) }, _ ->
          List (k1, [ phrasing e ], ba), acc1 :: acc2

      (* Paragraph *)
      (* End of the current paragraph has been reached and a new one is
       * started. *)
      | _, P _ when text = "\n" ->
          P ([], ba), acc1 :: acc2
      (* Continuation of the current paragraph. *)
      | _, P (phrasings, _) ->
          P ((phrasing e @ phrasings), ba), acc2

      (* Paragraph but formerly in a list: post-process the list items. *)
      | { list = (None | Some Lst.None) }, List _ ->
          P (phrasing e, ba), acc1 :: acc2

      (* Paragraph: everything else starts a new paragraph. *)
      | { blockquote = (None | Some false) }, Blockquote _
      | _, Header _
      | _, Empty ->
          P (phrasing e, ba), acc1 :: acc2

    (* Lists in Quill are complex beasts and need some post-processing. *)
    let finalise_list (k1, phrasings, ba1) =
      (* We always create an extra trailing empty element: remove it. *)
      let phrasings = try List.tl phrasings with _ -> phrasings in
      (* List items have been constructed backwards and both their content and
       * their own must now be reversed. *)
      let phrasings = List.rev_map List.rev phrasings in
      (* Return the list with that last element removed. *)
      Hierarchical.List (k1, phrasings, ba1)

    (* Main conversion function *)
    let translate t =
      (* First, make everything really fully linear. *)
      linearize t
      (* Fold over this to build the hierarchical representation. *)
      |> ListLabels.fold_left ~init:(Hierarchical.Empty, []) ~f:to_hierarchical
      (* The last element is not "commited" to the result set so do it now. *)
      |> (fun (acc1, acc2) -> acc1 :: acc2)
      (* Do some final adaptations. *)
      |> ListLabels.fold_left ~init:[] ~f:Hierarchical.(fun acc e ->
          match e with
          | Blockquote (phrasings, a) ->
              Blockquote (List.rev phrasings, a) :: acc
          | Header (n, phrasing, a) ->
              Header (n, phrasing, a) :: acc
          | List (k_e, phrasings_e, ba_e) ->
              finalise_list (k_e, phrasings_e, ba_e) :: acc
          | P (phrasings, a) ->
              P (List.rev phrasings, a) :: acc
          | Empty ->
              Empty :: acc
      )
  end
]
