(* Prose has a dumb client-server architecture. Each client has a local view
 * and model and sends changes done on the client back to the server which
 * merges and broadcasts them to all clients.
 *
 * The server is currently limited to two functions:
 * - relay edits to all other clients
 * - store, compose and send the result document to clients as they join
 *
 * Both client-side and server-side models are implemented through React
 * signals and events. Updates from and to clients are done through
 * Eliom_react.{Up,Down} values; in this context, "up" means it goes from the
 * client to the server and "down", the other way round.
 *
 * We first create an "up" channel which carries user changes to the server and
 * derive a React event from it.
 * Once we have a server-side React event for each client, we supplement it
 * with authorship information and create a "down" channel. We also fold over
 * its values in order to compose them and be able to feed new connectees with
 * the latest document immediately.
*)

[%%shared
  let websocket_port = 9001
  let websocket_resource_base = "/ws"
  let websocket_scheme = "wss"

  module type PConfig = sig
    val toolbar_id : string
    val quill_div : [> Html_types.div ] Eliom_content.Html.D.elt
    val quill_server_div : [> Html_types.div ] Eliom_content.Html.D.elt
    val quill_merger_div : [> Html_types.div ] Eliom_content.Html.D.elt
    val ourself : int
    val condensate_up : Prose_types.condensate Eliom_react.Up.t
    val pad_name : string
  end
]

[%%server
  open Prose_types

  module Storage = Prose_db

  let storage_get = Storage.(get path)

  let () =
    Random.self_init ()

  (* Fetch the data needed to re-build the most recent document verion from the
   * DB.
   *
   * First, fetch the most recent condensate for the document, then fetch the
   * individual edits that are more recent than the condensate. *)
  let backlog pad_name =
    (* Get the initial content for the given pad. Either we have some backlog
     * for it from a previous run, or we use the default welcome content. *)
    let storage = storage_get pad_name in
    match storage.Storage.find_last_condensate () with
    | exception _ -> 0L, None, []
    | Some (rowid, condensate) ->
        rowid, Some condensate, storage.Storage.find_edits_since rowid
    | None ->
        let rowid = 0L in
        rowid, None, storage.Storage.find_edits_since rowid

  (* Associate the author and edit ID to changes we receive.
   * Since he server logic is implemented with Lwt_react code, we first obtain
   * an Lwt_react value corresponding to the Ocsigen Eliom_react value with the
   * [to_react] function.
   * We also obtain the client ID with the eliom reference that we've created
   * above. Its value depends on the cilent-side process which the server
   * currently communicates with. As such the [current_client] reference
   * evaluates to the ID of the sender. *)
  let edit_event ~storage ~author up_event =
    up_event
    |> Lwt_react.E.map (fun (base_id, edit) ->
        storage.Storage.store_edit (base_id, author, edit);
        { base_id; author; edit }
      )

  (* Convert our React event to an Eliom_react value that can be used to send
   * data to clients. *)
   let down ~pad_egress_send edit_event =
     edit_event
     |> Lwt_react.E.map (fun e ->
         pad_egress_send (`Edit e)
     )

  module WS = struct
    open Websocket
    open Websocket_lwt

    type id = Prose_types.author

    type ws_up_msg =
      | Up of up
      | Close
      | Error of string

    let receive_wrapper e_send =
      fun f ->
        let open Websocket_cohttp_lwt.Frame in
        e_send @@ match f.opcode with
        | Opcode.Close ->
            Close
        | Opcode.Text ->
            Printf.eprintf "<= %s\n%!" f.content;
            let json = Yojson.Safe.from_string f.content in
            (match up_of_yojson json with
              | Result.Ok up ->
                  Up up
              | Result.Error msg ->
                  Error msg (* TODO: handle errors better *)
            )
        | Opcode.Ping ->
            prerr_endline "received ping";
            assert false
        | Opcode.Pong ->
            prerr_endline "received pong";
            assert false
        | _ ->
            prerr_endline "received something else";
            assert false

    let pad_events_get =
      let module PadEgressEvents = Ephemeron.K1.MakeSeeded (struct
        type t = string (* pad name * data *)
        let equal = (=)
        let hash = Hashtbl.seeded_hash
      end)
      in
      let wh = PadEgressEvents.create 17 in
      fun pad_name ->
        match PadEgressEvents.find wh pad_name with
        | x ->
            x
        | exception Not_found ->
            let (x_e, x_send) as x = React.E.create () in
            PadEgressEvents.add wh pad_name x;
            x

    let handler =
      let clients_data = Hashtbl.create 17 in
      fun
        (connection : Conduit_lwt_unix.flow * Cohttp.Connection.t)
        (request : Cohttp_lwt_unix.Request.t)
        (body : Cohttp_lwt.Body.t)
      ->
      let open Frame in
      let pad_name, author =
        let resource = Cohttp_lwt_unix.Request.(request.resource) in
        Printf.eprintf "Request URI: %s\n%!" resource;
        let prefix_length = String.length websocket_resource_base in
        Scanf.sscanf
          String.(sub resource prefix_length (length resource - prefix_length))
          "/%s@/%d"
          (fun pad_name author -> pad_name, author)
      in
      let%lwt () = Cohttp_lwt.Body.drain_body body in
      let ingress_events, ingress_send = Lwt_react.E.create () in
      let pad_egress_events, pad_egress_send = pad_events_get pad_name in
      let up =
        ingress_events
        |> Lwt_react.E.fmap (function
          | Close ->
              Hashtbl.remove clients_data (string_of_int author);
              None
          | Up (`Edit up_edit) ->
              Some up_edit
          | Up `GetBacklog ->
              pad_egress_send (`Backlog (backlog pad_name));
              None
        )
      in

      let storage = storage_get pad_name in
      let edit_event = edit_event ~author ~storage up in
      let down = down ~pad_egress_send edit_event in
      let%lwt (response, body, frames_out_fn) =
        Websocket_cohttp_lwt.upgrade_connection
          request
          (fst connection)
          (receive_wrapper ingress_send)
      in
      let pad_events_to_client =
        pad_egress_events
        |> Lwt_react.E.map (fun msg ->
            let content = msg |> down_to_yojson |> Yojson.Safe.to_string in
            Printf.eprintf "=> %s\n%!" content;
            frames_out_fn
            @@ Some (Websocket_cohttp_lwt.Frame.create ~content ())
        )
      in
      Hashtbl.add clients_data (string_of_int author) (up, down, pad_events_to_client);
      Lwt.return (response, (body :> Cohttp_lwt.Body.t))

    let () =
      ignore @@
        Cohttp_lwt_unix.Server.create
        ~mode:(`TCP (`Port websocket_port))
        (Cohttp_lwt_unix.Server.make ~callback:handler ())
  end

  (* We need some client-specific data, at least to get the authorship right.
   * We will use authorship so that when a client is notified of its own
   * changes by the server, it knows it is already up-to-date (provided no
   * other client has sent changes in between of course).
   *
   * The Eliom way to be able to have per-client data is called "scopes". Scopes
   * make it possible to have functions and values evaluate differently
   * depending on the chosen scope type. Here we use the default "process scope"
   * which means the evaluations will be constant across a given user-side
   * process, i.e. a tab. *)
  let scope = Eliom_common.default_process_scope

  let current_client =
    (* XXX: switch to nocrypto, at least because Random.int64 is >  *)
    Eliom_reference.Volatile.eref_from_fun ~scope Random.bits

  (* We need to send data of type "delta" from client to the server. We simply
   * create a service with the right type which have been created by the JSON
   * deriving PPX when we've defined the types.
   *
   * However, we don't directly use the eliom parameter value and instead go
   * through the functions of the Eliom_react module in order to create a value
   * of kind "up". The client will then be able to use it to push data to the
   * server.
   *
   * We also pass ~scope:`Site so that the value is always site-wide: the scope
   * argument defaults to `Client_process when outside of initialization but we
   * want to reuse the result for all clients. *)
  (* let up () =
    let ((up : up React.event), up_send as x) = React.E.create () in
    x
    (* Eliom_parameter.ocaml "up" [%derive.json: up]
    |> Eliom_react.Up.create ~scope:`Site *) *)

  (* Similarly, this function creates a way to send values of type
   * [condensate] from clients to the server. *)
  let condensate_up () =
    Eliom_parameter.ocaml "condensate_up" [%derive.json: condensate]
    |> Eliom_react.Up.create ~scope:`Site

  (* Clients regularly send "condensate" documents that can be used instead of
   * replaying each edit since the beginning. This stores them in the database.
   * Since the resulting signal is effectful-only and nothing depends on it, we
   * use Lwt_react.E.keep to prevent it from being garbage-collected. *)
  let store_condensates ~storage condensate_up =
    Eliom_react.Up.to_react condensate_up
    |> Lwt_react.E.map storage.Storage.store_condensate
    |> Lwt_react.E.keep

  (* Put together the various bits from above. We return a value that is made of
   * two main components. First, the client data that will be used to
   * communicate with the clients; it must be used in [%%client ...] blocks and
   * must also be re-created each time the server is re-started. Then, the
   * backlog which is a server-only value and is persistent across restarts. *)
  let pad_condensate_up pad_name =
    let storage = storage_get pad_name in
    let condensate_up = condensate_up () in
    store_condensates ~storage condensate_up;
    condensate_up
]

[%%client
module P (PConfig : PConfig) = struct
  open Prose_types
  open Prose_formats.Quill

  open PConfig

  type connection = {
    down : Opaque.t down_generic React.event;
    down_send : Opaque.t down_generic -> unit;
    up : up React.event;
    up_send : up -> unit;
  }

  type document_state = (Delta.edit, Delta.document) state_generic

  type state =
    | Disconnected of document_state option
    | Connected_Unsynchronised of (document_state option * connection)
    | Synchronised of (document_state * connection)

  let connection_signal, connection_send = React.S.create (Disconnected None)

  let set_panels_visibility ~o ~connection_state =
    let prose_disconnected = "prose-disconnected" in
    let prose_desynchronised = "prose-desynchronised" in
    let prose_connected = "prose-connected" in
    let prose_synchronised = "prose-synchronised" in
    let classes_add, classes_remove = match connection_state with
    | Disconnected _ ->
        [ prose_disconnected; prose_desynchronised ],
        [ prose_connected; prose_synchronised ]
    | Connected_Unsynchronised _ ->
        [ prose_connected; prose_desynchronised ],
        [ prose_disconnected; prose_synchronised ]
    | Synchronised _ ->
        [ prose_connected; prose_synchronised ],
        [ prose_disconnected; prose_desynchronised ]
    in
    List.iter (fun s -> o##.classList##add (Js.string s)) classes_add;
    List.iter (fun s -> o##.classList##remove (Js.string s)) classes_remove

  let time_get () =
    (new%js Js.date_now)##getTime

  let message_div, message_send =
    let message_signal, message_send = React.S.create None in
    let message_text = React.S.map (function Some s -> s | None -> "") message_signal in
    Eliom_content.Html.(D.(div [ span [ R.pcdata message_text ] ])), message_send

  let page_is_active, page_is_active_set = React.S.create true
  (* TODO: setup callback *)

  let connection () =
    let down, down_send = React.E.create () in
    let up, up_send = React.E.create () in
    {
      down; down_send;
      up; up_send;
    }

  (* TODO: wrap these behind accessors that reset their contents *)
  let quill = Prose_quill.setup ~toolbar_id quill_div
  let quill_server = Prose_quill.setup quill_server_div
  let quill_merger = Prose_quill.setup quill_merger_div

  (* We call the [on_down] function upon initialization to catch up with the
   * current document that the server has sent us. *)
  let t_make backlog =
    let epoch, condensate, edits = backlog in
    let t = Delta.empty_state in
    let last_full_document =
      (* NOTE: t.consensus is always at its default value, i.e. an empty
       * document so there's not much use in getting its values except that the
       * document value is already created. *)
      match condensate with
      | Some condensate -> Prose_formats.Quill.Delta.of_string condensate
      | None -> t.consensus
    in
    let history =
      CCFQueue.fold (fun history (base_id, author, s) ->
        let edit = { base_id; author; edit = Delta.of_string s } in
        CCFQueue.snoc history (Delta.rebase edit { t with history })
      ) CCFQueue.empty (CCFQueue.of_list edits)
    in
    let t = {
      consensus = Delta.compose_edits_on_document ~init:last_full_document ~q:history;
      epoch;
      history;
      local_deltas = CCFQueue.empty;
    }
    in
    quill_server##setContents t.consensus;
    quill_merger##setContents t.consensus;
    quill##setContents t.consensus;
    t

  let ws_channel_carries_edits ~connection ~ws =
    ws##.onmessage := Dom.handler (fun event ->
      let down = event##.data
        |> Js.to_string
        |> Yojson.Safe.from_string
        |> down_of_yojson
      in
      (match down with
      | Result.Ok (`Edit x) ->
          connection.down_send x
      | Result.Ok _ ->
          Lwt_log_js.ign_fatal_f "Couldn't parse data from server: %s" "wrong type"
      | Result.Error msg ->
          Lwt_log_js.ign_fatal_f "Couldn't parse data from server: %s" msg);
      Js._false
    )

  let ws_setup ~connection ~ws =
    ws##.onmessage := Dom.handler (fun event ->
      let down = event##.data
        |> Js.to_string
        |> Yojson.Safe.from_string
        |> down_of_yojson
      in
      (match down with
      | Result.Ok (`Backlog backlog) ->
          let t = t_make backlog in
          ws_channel_carries_edits ~connection ~ws;
          connection_send (Synchronised (t, connection));
      | Result.Ok _ ->
          Lwt_log_js.ign_fatal_f "Couldn't parse data from server: %s" "wrong type"
      | Result.Error msg ->
          Lwt_log_js.ign_fatal_f "Couldn't parse data from server: %s" msg);
      Js._false
    )

  let ws_connect ~pad_name ~ourself =
    let uri = Printf.sprintf "%s://%s:%d%s/%s/%d"
      websocket_scheme
      (Js.to_string Dom_html.window##.location##.hostname)
      (if websocket_scheme = "wss" then 443 else websocket_port)
      websocket_resource_base
      pad_name
      ourself
    in
    let connection = connection () in
    let ws = new%js WebSockets.webSocket (Js.string uri) in
    ws##.onclose := Dom.handler (fun _ ->
      connection_send (Disconnected None);
      Js._false);
    ws_setup ~ws ~connection;
    connection.up
      |> Lwt_react.E.map (fun up ->
          let s = up |> up_to_yojson |> Yojson.Safe.to_string in
          Lwt_log_js.ign_debug_f  "<- %s\n" s;
          ws##send (Js.string s)
        )
      |> Lwt_react.E.keep;
    ws##.onopen := Dom.handler (fun _ ->
      connection_send (Connected_Unsynchronised (None, connection));
      Js._false)

  (* Wrap [f arg1 arg2] in order to log exceptions. *)
  let with_backtrace f arg1 arg2 =
    (* TODO: warn the user *)
    try f arg1 arg2 with exn ->
      ignore (Lwt_log_js.fatal_f "Exception: %s" (Printexc.to_string exn));
      raise exn

  (* Number of edits between two condensates on the server. *)
  let condensate_upload_period = 10L

  (* We have all the tools to create the client-side elements now. The only data
   * we have to get from the server before doing so is the author name for the
   * current client and the current document state (i.e. backlog). *)

  let edit_event, edit_event_send = Lwt_react.E.create ()

  (* Upload the current consensus document if we are the author of the most
   * recent edit and its ID modulo [condensate_upload_period] equals 0. *)
  let upload_condensate t =
    let edit_id = id_of_state t in
    let _, { author } = CCFQueue.take_back_exn t.history in
    if author = ourself && Int64.rem edit_id 10L = 0L then
      ignore @@ condensate_up (edit_id, Delta.to_string t.consensus)
    else
      ()

  (* Update the model according to [edit].
   * This implies first rebasing the edit on top of the most recent document,
   * then pushing it to our history and composing the consensus with it.
   * Second step is to rebase the edits that are still local on top of it.
   * Returns a new model updated with all of the above. *)
  let update_model ~edit ~t_old =
    Lwt_log_js.ign_debug_f "-> [%Ld, %s]\n" edit.base_id Js.(to_string (_JSON##stringify edit.edit));
    let edit = Delta.rebase edit t_old in
    let t = { t_old with
      consensus = t_old.consensus##compose edit.edit;
      history = CCFQueue.snoc t_old.history edit
    }
    in
    let local_deltas =
      let t1 = time_get () in
      (if edit.author <> ourself then
        t.local_deltas
      else
        let (t0, _edit), local_deltas = CCFQueue.take_front_exn t.local_deltas in
        Lwt_log_js.ign_debug_f "Received server ack in %f seconds.\n" ((t1 -. t0) /. 1000.);
        local_deltas
      )
      |> CCFQueue.map (fun (t0, edit) ->
          let p1 = Js.(to_string (_JSON##stringify edit.edit)) in
          let edit' = Delta.rebase edit t in
          let p2 = Js.(to_string (_JSON##stringify edit'.edit)) in
          (if p1 <> p2 then (
            Lwt_log_js.ign_debug_f
              "  [%Ld, %s] => [%Ld, %s]\n"
              edit.base_id p1
              edit'.base_id p2
          ));
          t0, edit'
      )
    in
    { t with local_deltas }

  (* Update the view according to the changes in the model. *)
  let update_view ~old_edit ~t_old ~t_new =
    (* Get the most recent edit (after it was rebased). *)
    (* let edit = fst (CCFQueue.take_front_exn t_new.history) in *)
    (* Touching the view is costly and therefore we try to optimize.
     * First, if we are the author of the last edit and the rebase didn't
     * modify it, we don't have anything to do. *)
    (* if edit.author = ourself then
      && edit = old_edit then

      if t.local_deltas = CCFQueue.empty then
        ignore (quill##updateContents edit.edit)
      else
        (* Save the current selection. *)
        let selection = quill##getSelection () in
        quill##setContents (Delta.compose_edits_on_document ~init:t.consensus ~q:t.local_deltas)
        ignore @@ Js.Opt.map selection (fun x ->
          quill##setSelection
            (Selection.rebase ~state:t ~ourself ~old_edit.base_id x))
    else
      () *)
    (* Apply the edit to the view.
     * This is a costly operation: completely resetting the view can take
     * several milliseconds; updating the view with one edit is twice as fast
     * (but that's still quite costly. *)
    let current =
      let q = CCFQueue.map (fun (_t0, e) -> e) t_old.local_deltas in
      Delta.compose_edits_on_document ~init:t_old.consensus ~q
    in
    let next =
      let q = CCFQueue.map (fun (_t0, e) -> e) t_new.local_deltas in
      Delta.compose_edits_on_document ~init:t_new.consensus ~q
    in
    let diff =
      try current##diff next with exn ->
        (* TODO: display a message telling the user that something has gone
         * wrong, cannot be recovered from and that creating a new pad is
         * required. *)
        Firebug.console##log_4 t_old t_new current next;
        raise exn
    in
    (* let get_i x i =
      Js.Optdef.get (Js.array_get x##.ops i) (fun _ -> assert false)
    in
    let equal a b =
      let h x y =
        x##.insert = y##.insert
        && x##.retain = y##.retain
        && x##.delete = y##.delete
      in
      let open Js in
      let la = a##.ops##.length in
      let lb = b##.ops##.length in
      if la = lb then
        let ret = ref true in
        for i = 0 to la - 1 do
          ret := !ret && h (get_i a i) (get_i b i)
        done;
        !ret
      else
        false
    in
    let s = (if not @@ equal (current##compose diff) next then
      "fuuu"
    else
      "ok")
    in
    Firebug.console##log_5 s current##.ops next##.ops diff##.ops (current##compose diff)##.ops; *)
    ignore (quill_server##setContents t_new.consensus);
    ignore (quill_merger##updateContents diff);
    ignore (quill##updateContents diff)

  let on_down t_old ({ base_id = old_base } as edit) =
    let t_new = update_model ~edit ~t_old in
    upload_condensate t_new;
    update_view ~old_edit:edit ~t_new ~t_old;
    t_new

(*
* XXX: remove
  let on_down t_old l =
    l
    |> List.fold_left (fun t_old ({ base_id = old_base } as edit) ->
        let t_new = update_model ~edit ~t_old in
        upload_condensate t_new;
        update_view ~old_edit:edit ~t_new ~t_old;
        t_new
      )
      t_old
    |> Lwt.return
  in
  *)

  let server_message_of_edit edit =
    `Edit (edit.base_id, Js.(to_string (_JSON##stringify edit.edit)))

  (* We also need a callback for text-change events in order to store changes
   * and send them to the server.
   * Callbacks for Quill accept three arguments: [edit], [base] and
   * [source]. The first one is the change itself, the second is the document
   * on which it applies so that composing [edit] on top of [base] gives the
   * new document and the last one indicates what has caused the change: it
   * is often "user" or "api". *)
  let record_change t (edit : Delta.edit) =
    let edit = { base_id = id_of_state t; edit; author = ourself } in
    let local_deltas = CCFQueue.snoc t.local_deltas (time_get (), edit) in
    quill_merger##updateContents edit.edit;
    (match React.S.value connection_signal with
    | Disconnected _ -> ()
    | Connected_Unsynchronised _ -> () (* NOTE: shouldn't be allowed to happen *)
    | Synchronised (_, connection) -> connection.up_send (server_message_of_edit edit));
    { t with local_deltas }

  let on_text_change (edit : Delta.edit) _base source =
    (* We only care about events done through user interaction currently .*)
    Lwt.return
    @@
      if Js.to_string source = "user" then
        edit_event_send @@ `TextChange edit
      else
        (* If the event was not user-generated, do nothing. *)
        ()

  let handle_events t = function
    | `TextChange edit -> record_change t edit
    | `Down { Prose_types.base_id; author; edit } -> on_down t {
        base_id = base_id;
        author = author;
        edit = Delta.of_string edit;
      }

  (* Now that the callback has been defined, we register it on the
   * "text-change" signal.
   * We use the [quill] JS object and its method [on]. However, this is
   * obviously a JS method and we need to convert its arguments to something
   * that JS will understand directly. The "text-change" string is converted
   * to a JS string (i.e. UTF-16) and the [on_text_change] callback is wrapped
   * through Js.wrap_callback. *)
  let () = quill##on (Js.string "text-change") (Js.wrap_callback on_text_change)

  let switch_with_strong_stop f s =
    let open React.E in
    s
    |> fold (fun old_s e ->
        may (stop ~strong:true) old_s;
        Some (f e)
    ) None

  let downs_send =
    let downs, downs_send = React.E.create () in
    (* map *)
    switch_with_strong_stop (React.E.map (fun down -> edit_event_send (`Down down))) downs
    |> Lwt_react.E.keep;
    downs_send

  let document_states_send =
    let document_states, document_states_send = React.E.create () in
    (* fold *)
    switch_with_strong_stop (fun ds ->
      React.E.fold (with_backtrace handle_events) ds edit_event) document_states
    |> Lwt_react.E.keep;
    document_states_send

  let on_connection_state_change new_state old_state =
    let s_of_state = function
      | Synchronised _ -> "sync"
      | Connected_Unsynchronised _ -> "conn-unsync"
      | Disconnected _ -> "disconnected"
    in
    let o = Dom_html.getElementById "os-body" in
    Lwt_log_js.ign_notice_f "ws: %s (old was %s)"
      (s_of_state new_state) (s_of_state old_state);
    set_panels_visibility ~o ~connection_state:new_state;
    match old_state, new_state with
    | Disconnected _, Disconnected _ ->
        ws_connect ~pad_name ~ourself
    | Synchronised (_document_state, _connection), Disconnected _ ->
        message_send (Some "Websocket connection has closed, refresh the page. Any further edit will be lost.");
        ws_connect ~pad_name ~ourself
    | Disconnected _, Connected_Unsynchronised (_, connection) ->
        connection.up_send `GetBacklog
    | Connected_Unsynchronised _, Synchronised (document_state, connection) ->
        downs_send connection.down;
        document_states_send document_state
    | _, _ ->
        ()

  let () =
    let open Lwt_react in
    S.diff on_connection_state_change connection_signal
    |> E.keep;
    on_connection_state_change (S.value connection_signal) (S.value connection_signal)

  let download_from_data data =
    let constructor = Js.Unsafe.global##._Blob in
    let js_data = new%js Js.array_empty in
    ignore (js_data##push data);
    let blob = new%js constructor js_data Js.undefined in
    let url = Dom_html.window##._URL##createObjectURL blob in
    Dom_html.window##.location##assign url

  let click_to_download_from_data element data_get converter =
    ignore @@ Lwt_js_events.clicks
      (Eliom_content.Html.To_dom.of_element element)
      (fun _event _ -> Lwt.return (
        download_from_data (converter (data_get ()))
      ))
end
]

[%%server
  open Eliom_content.Html

  (* The function below creates the pages that are sent to clients. It is the
   * first place where we can do some things because code earlier was executed
   * in a global context while this will run in a per-client context.
   *
   * To be honest, I'm not sure what [user_id] is and what it can be used for.
   *
   * Overall, we do fairly little here besides putting everything together that
   * we've defined above. *)
  let pads_service_handler user_id pad_name () =
    (* We dereference [current_client] at a time where the context for the
     * current client is defined. Obviously, outside of any request, the scope
     * we are using is not defined because it is per client-side process. *)
    let current_client = Eliom_reference.Volatile.get current_client in

    let condensate_up = pad_condensate_up pad_name in

    (* Define UI elements. *)
    let q, editors_div, toolbar_div =
      let toolbar_id = "quill-toolbar-main" in
      let quill_div = D.(div ~a:[ a_class [ "quill"; "editor" ] ] []) in
      let quill_server_div = D.(div ~a:[ a_class [ "quill"; "server" ] ] []) in
      let quill_merger_div = D.(div ~a:[ a_class [ "quill"; "merger" ] ] []) in
      let editors_div =
        D.(div ~a:[ a_class [ "editor_group" ] ] [
          quill_div;
          quill_server_div;
          quill_merger_div;
        ])
      in
      let toolbar_div = Prose_quill.Toolbar.(toolbar ~toolbar_id options) in
      let q = [%client (
        (* This builds the JS Quill object and does basically nothing
         * ocsigen-specific. *)
        (* We need to send them to the client. This is done with the help of
         * the [%client ...] syntax in server-side blocks: it evaluates the
         * corresponding code on the client even though we are in a server
         * block.
         * We only have to call the function we've defined for the
         * initialization in the client block above and pass it the values as
         * references that it can pull later one from the server through the
         * ~% syntax or directly. Note that if we didn't use ~% here,
         * compilation would error out with an "unbound value" error.
         * The syntax is slightly heavy and we have to specify the return
         * type of the computation with ": unit" because it can't be done
         * automatically. *)
        (* XXX: this should be kept as long as the client is alive *)
        let module PP = P(struct
          let toolbar_id = ~%toolbar_id
          let quill_div = ~%quill_div
          let quill_merger_div = ~%quill_merger_div
          let quill_server_div = ~%quill_server_div
          let condensate_up = ~%condensate_up
          let ourself = ~%current_client
          let pad_name = ~%pad_name
        end)
        in
        PP.quill : Prose_formats.Quill.Quill.t)
      ] in
      q, editors_div, toolbar_div
    in
    (* let exports_div =
      D.div (
        let open Eliom_content.Html in
        let quill_export = D.(Raw.a [ pcdata "Raw Quill" ]) in
        let html_export = D.(Raw.a [ pcdata "HTML" ]) in
        ignore [%client ((
          let open PP in
          let mk element converter =
            click_to_download_from_data element (fun () -> ~%q##getContents ()) converter
          in
          mk ~%quill_export (fun o -> Js._JSON##stringify o);
          mk ~%html_export (fun document ->
            let open Prose_formats in
            Quill.Quill.to_delta document
            |> ToHierarchical.translate
            |> HTML.to_html
            |> Eliom_content.Html.F.div
            |> (fun content -> (Eliom_content.Html.To_dom.of_element content)##.outerHTML)
          )
        ) : unit) ];
        F.[ div ~a:[ a_class [ "exports" ] ] [
          span [ pcdata "Exports" ];
          ul [
            li [ quill_export ];
            li [ html_export ];
          ]
        ] ]
      );
    in *)
    (* let about =
        Some D.(div ~a:[ a_class classes ] [ Prose_welcome.about ])
    in *)

    let ui =
      [
        (* Eliom_content.Html.C.node [%client PP.message_div]; *)
        toolbar_div;
        editors_div;
        (* exports_div; *)
      ]
    in

    (* This is how we ultimately build the page itself. We simply put two
     * elements in the central block:
       * quill.js
       * a div named "quill-editor" where the Quill client-side instance will be
       *   setup
     * We also provide a page title based on the pad name. *)
    Lwt.return (
      Os_page.content
        ~title:(pad_name ^ " - Prose")
        ~head:[ Prose_quill.include_quill; ]
        [ F.(div ~a:[ a_class ["os-body"]; a_id "os-body" ] ui) ]
    )

]

(* Register the service under /pads. It takes a "pad=pad_name" parameter which
 * can be provided by accessing /pads/pad_name. *)
let pads_service =
  let pad_name = Eliom_parameter.string "pad" in
  let parameters = Eliom_parameter.suffix pad_name in
  Eliom_service.(create
    ~path:(Path ["pads"])
    ~meth:(Get parameters)
    ()
  )

(* Put the "default" pad at the root so that / lands people on the same pad as
 * "/pads/default". *)
let main_service_handler_default user_id () () =
  pads_service_handler user_id "default" ()

(* Last step: register this module so that incoming client requests can be
 * dispatched to it. *)
module Prose_app = Eliom_registration.App (struct
  let application_name = "prose"
  let global_data_path = None
end)

(* The code below has been generated by ocsigen-start. It registers all the
 * services with their respective handler.
 *
 * The let%server is a syntax extension to define server code. It is a shorthand to putting everything in a [%%server ...] block. *)
let%server () =
  (* Register our handler for the [pads_service] we've defined above. *)
  Prose_base.App.register
    ~service:pads_service
    (Prose_page.Opt.connected_page pads_service_handler);

  (* Also register a handle for the default [Os_services.main_service] service.
   * I haven't been able to just modify it so far and it takes no parameter so
   * instead I've decided to make it use default values, i.e. make it the same
   * as the pad named "default". *)
  Prose_base.App.register
    ~service:Os_services.main_service
    (Prose_page.Opt.connected_page main_service_handler_default);

  Prose_base.App.register
    ~service:Prose_services.about_service
    (Prose_page.Opt.connected_page Prose_handlers.about_handler)


(* Print more debugging information when <debugmode/> is in config file
   (DEBUG = yes in Makefile.options).
   Example of use:
   let section = Lwt_log.Section.make "Prose:sectionname"
   ...
   Lwt_log.ign_info ~section "This is an information";
   (or ign_debug, ign_warning, ign_error etc.)
 *)
let%server _ =
  ignore
    [%client ((
      let open Lwt_log_core in
      Section.(set_level main) Debug;
      add_rule "Prose*" Debug;
    ): unit)];
  let open Lwt_log_core in
  Section.(set_level main) Debug;
  add_rule "Prose*" Debug
