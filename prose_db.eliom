module Sqlexpr = Sqlexpr_sqlite.Make(Sqlexpr_concurrency.Id)
module S = Sqlexpr

let path = "prose_pads.db"

let open_db path =
  S.open_db path

let table_name pad_name =
  "pads_" ^ pad_name

let sp = Printf.sprintf

let table_exists db name =
  try
    ignore
    @@ S.select_one
      db
      [%sql "SELECT @s{name} FROM sqlite_master WHERE type = 'table' and name = %s"]
      (table_name name);
    true
  with _ -> false

let create_table db name =
  let sql = sp "CREATE TABLE %s (base_id BIGINT, author INTEGER, edit BLOB NOT NULL, condensate BLOB)" (table_name name) in
  S.execute
    db
    { S.sql_statement = sql; stmt_id = None; directive = S.Directives.literal sql }

let find_last_condensate db name =
  let sql = sp "SELECT rowid, condensate FROM %s WHERE condensate NOT NULL ORDER BY rowid DESC LIMIT 1" (table_name name) in
  fun () ->
    S.(select_one_maybe
      db
      {
        statement = { sql_statement = sql; stmt_id = None; directive = Directives.literal sql };
        get_data = 2, (fun a ->
          Conversion.(int64 a.(0), blob a.(1))
        )
      }
    )

let find_edits_since db name =
  let sql = sp "SELECT base_id, author, edit FROM %s WHERE rowid > ? ORDER BY rowid ASC" (table_name name) in
  let select =
    S.(select
      db
      {
        statement = { sql_statement = sql; stmt_id = None; directive = Directives.int64 };
        get_data = 3, (fun a ->
          Conversion.(int64 a.(0), int a.(1), blob a.(2))
        )
      }
    )
  in
  fun since -> select since

let store_edit db name =
  let sql = sp "INSERT INTO %s (base_id, author, edit) VALUES(?, ?, ?)" (table_name name) in
  let insert =
    S.insert
      db
      { S.sql_statement = sql; stmt_id = None; directive = (fun k1 k2 -> S.Directives.(int64 (int (blob k1)) k2)) }
  in
  fun (base_id, author, edit) -> insert base_id author edit

let store_condensate db name =
  let sql = sp "UPDATE %s SET condensate = ? WHERE rowid = ? AND condensate IS NULL" (table_name name) in
  let update =
    S.execute
      db
      { S.sql_statement = sql; stmt_id = None; directive = (fun k1 k2 -> S.Directives.(blob (int64 k1) k2)) }
  in
  fun (rowid, condensate) -> ignore @@ update condensate rowid

type t = {
  store_edit : (int64 * int * string) -> int64;
  store_condensate : (int64 * string) -> unit;
  find_edits_since : int64 -> (int64 * int * string) list;
  find_last_condensate : unit -> (int64 * string) option;
}

let with_timing n f =
  fun x ->
    let t0 = Unix.gettimeofday () in
    let ret = f x in
    let t1 = Unix.gettimeofday () in
    Printf.eprintf "[%S] %f\n%!" n (t1 -. t0);
    ret

let storage db pad_name =
  let pad_name = B64.(encode ~pad:false ~alphabet:uri_safe_alphabet pad_name) in
  (if not (table_exists db pad_name) then
    create_table db pad_name);
  {
    store_edit = store_edit db pad_name;
    store_condensate = store_condensate db pad_name;
    find_edits_since = find_edits_since db pad_name;
    find_last_condensate = find_last_condensate db pad_name;
  }

let get path =
  let db = open_db path in
  let sql = "PRAGMA synchronous = normal" in
  S.(execute db { sql_statement = sql; stmt_id = None; directive = S.Directives.literal sql });
  let sql = "PRAGMA journal_mode = wal" in
  S.(execute db { sql_statement = sql; stmt_id = None; directive = S.Directives.literal sql });
  let h = Hashtbl.create 10 in
  fun pad_name ->
    match Hashtbl.find h pad_name with
    | exception Not_found ->
        let storage = storage db pad_name in
        Hashtbl.add h pad_name storage;
        storage
    | storage -> storage
